#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import sortmusic

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')


class TestOrderItems(unittest.TestCase):

    def test_order_descending(self):
        songs = [("song1", 10), ("song2", 5), ("song3", 20)]
        result = sortmusic.order_items(songs, 1, 1)
        expected = [("song1", 10), ("song2", 5), ("song3", 20)]
        self.assertListEqual(result, expected)

    def test_order_same_reproductions(self):
        songs = [("song3", 20), ("song2", 10), ("song1", 10), ("song4", 35)]
        result = sortmusic.order_items(songs, 3, 2)
        expected = [("song3", 20), ("song4", 35), ("song1", 10), ("song2", 10)]
        self.assertListEqual(result, expected)


class TestSortMusic(unittest.TestCase):

    def test_sort_descending(self):
        songs = [("song1", 10), ("song2", 5), ("song3", 20)]
        result = sortmusic.sort_music(songs)
        expected = [("song3", 20), ("song1", 10), ("song2", 5)]
        self.assertListEqual(result, expected)

    def test_sort_same_reproductions(self):
        songs = [("song1", 10), ("song2", 10), ("song3", 20)]
        result = sortmusic.sort_music(songs)
        expected = [("song3", 20), ("song1", 10), ("song2", 10)]
        self.assertListEqual(result, expected)

    def test_3(self):
        songs = [("song1", 10), ("song2", 5), ("song3", 20)]
        result = sortmusic.sort_music(songs)
        expected = [("song3", 20), ("song1", 10), ("song2", 5)]
        self.assertEqual(result, expected)

    def test_4(self):
        songs = [("song1", 10)]
        result = sortmusic.sort_music(songs)
        self.assertEqual(result, [("song1", 10)])


class TestCreateDictionary(unittest.TestCase):

    def test_create_dictionary(self):
        arguments = ["song1", "10", "song2", "5", "song3", "20"]
        result = sortmusic.create_dictionary(arguments)
        expected = {"song1": 10, "song2": 5, "song3": 20}
        self.assertEqual(result, expected)

    def test_invalid_arguments_order(self):
        arguments = ["10", "song1", "song2", "50", "17", "song3"]
        with self.assertRaises(SystemExit):
            sortmusic.create_dictionary(arguments)

    def test_non_integer_reproductions(self):
        arguments = ["song1", "10", "song2", "5", "song3", "twenty"]
        with self.assertRaises(SystemExit):
            sortmusic.create_dictionary(arguments)


class TestMain(unittest.TestCase):

    def test_simple(self):
        expected = "{'song3': 20, 'song1': 10, 'song2': 5}\n"
        stdout = StringIO()
        with patch.object(sys, 'argv', ['test_script.py', 'song1', '10', 'song2', '5', 'song3', '20']):
            with contextlib.redirect_stdout(stdout):
                sortmusic.main()
            output = stdout.getvalue()
            self.assertEqual(output, expected)

    def test_incomplete_arguments(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['test_script.py', 'song1', '10', 'song2', '5', 'song3']):
            with self.assertRaises(SystemExit):
                sortmusic.main()

    def test_empty_arguments(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['test_script.py']):
            with self.assertRaises(SystemExit):
                sortmusic.main()


if __name__ == '__main__':
    unittest.main()
